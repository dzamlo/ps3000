{ stdenv, makeWrapper, lib, shellcheck, shfmt }:
  stdenv.mkDerivation {
    name = "lint-script";
    src = ./lint.sh;

    buildInputs = [ makeWrapper ];

    dontUnpack = true;

    installPhase = ''
      runHook preInstall
      mkdir -p $out/bin
      cp $src $out/bin/lint.sh
      chmod +x $out/bin/lint.sh
      wrapProgram $out/bin/lint.sh --prefix PATH : ${lib.makeBinPath [ shellcheck shfmt ]}
      runHook postInstall
    '';
  }
