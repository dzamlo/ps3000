#!/bin/sh
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
#
# Lint the bash files in the repo

set -e
shellcheck --version
shellcheck -- *.sh
printf "shfmt: "
shfmt -version
shfmt -i 4 -ci -d .
