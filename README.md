# PS3000

PS3000, because PS1 wasn't cool enough!

This is a simple bash prompt that provide useful information. Its main goal is to
be simple to understand, easy to deploy (single file, limited dependency) and
works in most environment where bash run (no emoji or other fancy unicode
things)

## Usage

Source `ps3000.sh` in your `.bashrc`.

On most system, you can use the following commands to install PS300:
```
git clone https://gitlab.com/dzamlo/ps3000.git "$HOME/.ps3000"
echo 'source "$HOME/.ps3000/ps3000.sh"' >> "$HOME/.bashrc"
```

## Personalization

You can adjust the PS3000_THEME, PS3000_COMMANDS, PROMPT_DIRTRIM and
PS3000_GIT_TIMEOUT variables

## Development environment

To get a reproducible development environment we use `devenv.sh`. To get started follow the installation instructions on [https://devenv.sh](https://devenv.sh). You can then run `devenv shell` to get a shell with the necessary tools. You can then run the `./lint.sh` script to lint the shell scripts in the repo.
