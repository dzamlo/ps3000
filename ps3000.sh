#!/bin/bash
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
#
# PS3000, because PS1 wasn't cool enough!
#
# This is a simple bash prompt that provide useful information.
# Its main goal is be to simple to understand, easy to deploy (single file,
# limited dependency) and works in most environment where bash run (no emoji
# or other fancy unicode things)
#
# Usage: source this file in your .bashrc
#
# Personalization: you can adjust the PS3000_THEME, PS3000_COMMANDS,
# PROMPT_DIRTRIM and PS3000_GIT_TIMEOUT variables
#

function ps3000__git_detached() {
    echo -n "("
    # in detached head, the output for the current branch of `git branch` looks like "* (some text)"
    # we want to get the "some text" part
    echo -n "$(git branch --no-color | sed -nE 's/^\* \((.*)\)/\1/p')"

    # We get all the refs that match exactly
    local refs
    refs=$(git for-each-ref --points-at=HEAD --format="%(refname:short)" 2>/dev/null)
    if ! refs=$(git for-each-ref --points-at=HEAD --format="%(refname:short)" 2>/dev/null); then
        echo -n "no initial commit yet"
    elif [[ -n "$refs" ]]; then
        echo -n ", refs: ${refs//$'\n'/, }"
        echo -n ", $(git rev-parse --short HEAD)"
    else
        # no refs matched exactly, we resort to using git describe to at least get some idea of where we are
        local describe_before describe_after
        describe_before=$(git describe --all 2>/dev/null || true)
        describe_after=$(git describe --all --contains 2>/dev/null || true)
        if [[ -n $describe_before ]] && [[ $describe_before == "${describe_after%^0}" ]]; then
            # if before and after are the same (discarding an eventual ^0 at the end of after) and not empty, add only one
            # this may happen if the commit match exactly a ref.
            echo -n ", $describe_before"
        else
            # before and after don't match or are both empty, we add the non empty ones.
            if [[ -n $describe_before ]]; then
                echo -n ", $describe_before"
            fi
            if [[ -n $describe_after ]]; then
                echo -n ", $describe_after"
            fi
        fi
    fi

    echo -n ")"
}

function ps3000__git_print_number() {
    prefix="$1"
    value="$2"
    if [[ "$value" -ne 0 ]]; then
        echo -n "$warning_color"
    fi
    echo -n "$prefix$value"
    echo -n "$normal_color"

}

function ps3000__git() {
    # shellcheck disable=2030
    (
        set -euo pipefail
        normal_color="$1"
        warning_color="$2"
        git_ahead="$3"
        git_behind="$4"
        git_conflict="$5"
        git_modified="$6"
        git_deleted="$7"
        git_staged="$8"
        git_stash="$9"
        git_untracked="${10}"

        # If git take the index lock and is killed because of timeout, the lock isn't freed and this cause issue
        export GIT_OPTIONAL_LOCKS=0

        local git_branch
        git_branch=$(git rev-parse --abbrev-ref HEAD 2>/dev/null || true)

        if [[ -n "$git_branch" ]]; then

            if [[ -n "$(git status --porcelain 2>/dev/null)" ]]; then
                echo -n "$warning_color"
            fi

            if [[ "$git_branch" == "HEAD" ]]; then
                # We are in detached HEAD
                # instead of the branch name and git describe, we print a more complete description
                # Everything included in describe is included. Printing the describe would be redundant
                echo -n "$(ps3000__git_detached)"
            else
                echo -n "$git_branch"
                local describe
                # We don't use --dirty because
                # a) We show the dirty state with the color anyway
                # b) with --dirty, git describe take the index lock and if the command get killed because of a timeout, the lock isn't freed
                describe="$(git describe --long --tags --always 2>/dev/null || true)"
                if [[ -n "$describe" ]]; then
                    echo -n " $describe"
                fi
            fi

            echo -n "$normal_color"

            if [[ -n "$(git rev-parse --show-superproject-working-tree)" ]]; then
                echo -n " ${warning_color}inside submodule$normal_color"
            fi

            local behind_ahead ahead behind
            # If this command fail this means there is no remote. in this case we just do not print the ahead/behind
            if behind_ahead="$(git rev-list --count --left-right '@{upstream}...HEAD' 2>/dev/null)"; then
                ahead="${behind_ahead#*	}"
                behind="${behind_ahead%	*}"
                echo -n " "
                if [[ "$ahead" -ne 0 ]] || [[ "$behind" -ne 0 ]]; then
                    echo -n "$warning_color"
                fi
                echo -n "$git_ahead$ahead $git_behind$behind"
                echo -n "$normal_color"
            fi

            if [[ "$(git rev-parse --is-inside-work-tree 2>/dev/null)" == "true" ]]; then
                local staged
                staged="$(git diff --staged --name-only --cached -z | grep -zc '' || true)"
                echo -n " "
                ps3000__git_print_number "$git_staged" "$staged"

                local modified
                modified="$(git diff --name-only --diff-filter=M -z | grep -zc '' || true)"
                echo -n " "
                ps3000__git_print_number "$git_modified" "$modified"

                local deleted
                deleted="$(git diff --name-only --diff-filter=D -z | grep -zc '' || true)"
                echo -n " "
                ps3000__git_print_number "$git_deleted" "$deleted"

                local untracked
                untracked="$(git status --porcelain=2 -z | grep -zc '^?' || true)"
                echo -n " "
                ps3000__git_print_number "$git_untracked" "$untracked"

                local stash
                stash="$(git stash list 2>/dev/null | wc -l)"
                echo -n " "
                ps3000__git_print_number "$git_stash" "$stash"

                local conflicts
                conflicts="$(git diff --name-only --diff-filter=U -z | grep -zc '' || true)"
                echo -n " "
                ps3000__git_print_number "$git_conflict" "$conflicts"
            fi
        fi
    )
}

function ps3000_git() {
    export -f ps3000__git
    export -f ps3000__git_detached
    export -f ps3000__git_print_number
    # shellcheck disable=2031
    timeout "$PS3000_GIT_TIMEOUT" bash -c 'ps3000__git "$@"' ps3000__git "${PS3000_THEME[normal]}" "${PS3000_THEME[warning]}" "${PS3000_THEME[git_ahead]}" "${PS3000_THEME[git_behind]}" "${PS3000_THEME[git_conflict]}" "${PS3000_THEME[git_modified]}" "${PS3000_THEME[git_deleted]}" "${PS3000_THEME[git_staged]}" "${PS3000_THEME[git_stash]}" "${PS3000_THEME[git_untracked]}"
}

function ps3000_git_index_locked() {
    if [[ -f "$(git rev-parse --git-dir 2>/dev/null)/index.lock" ]]; then
        echo "${PS3000_THEME[error]}git index locked!!!"
    fi
}

function ps3000_jobs() {
    if [[ "$ps3000__nb_jobs" -ne 0 ]]; then
        echo "${PS3000_THEME[jobs]}$ps3000__nb_jobs"
    fi
}

function ps3000_read_only() {
    if [[ ! -w "$PWD" ]]; then
        echo "${PS3000_THEME[read_only]}"
    fi
}

function ps3000_return_code() {
    if [[ "$ps3000__return_code" -ne 0 ]]; then
        echo "${PS3000_THEME[return_code]}$ps3000__return_code"
    fi
}

function ps3000_shell_level() {
    if [[ "$SHLVL" -ne 1 ]]; then
        echo "${PS3000_THEME[shell_level]}$SHLVL"
    fi
}

function ps3000_sudo_cached() {
    if sudo -n true 2>/dev/null; then
        echo "${PS3000_THEME[sudo_cached]}"
    fi
}

function ps3000_venv() {
    if [[ -n "$VIRTUAL_ENV" ]]; then
        echo "${PS3000_THEME[venv]}$(basename "$(dirname "$VIRTUAL_ENV")")/$(basename "$VIRTUAL_ENV")"
    fi
}

function ps3000_execution_time() {
    if [[ -n "$ps3000__start_time" ]] && [[ -n "$ps3000__end_time" ]]; then
        local execution_time_seconds=$(("$ps3000__end_time-$ps3000__start_time"))
        if ((execution_time_seconds >= 2)); then
            local execution_time_minutes=$((execution_time_seconds / 60))
            execution_time_seconds=$(printf %02d $((execution_time_seconds % 60)))
            echo "${PS3000_THEME[execution_time]}$execution_time_minutes:$execution_time_seconds"
        fi
    fi
}

function ps3000_battery() {
    local display_threshold="$1"
    local medium_threshold="$2"
    local low_threshold="$3"
    local percentage
    percentage="$(cat /sys/class/power_supply/BAT0/capacity 2>/dev/null || true)"
    if [[ -n "$percentage" ]]; then
        if ((percentage <= low_threshold)); then
            echo -n "${PS3000_THEME[battery_low]}"
        elif ((percentage <= medium_threshold)); then
            echo -n "${PS3000_THEME[battery_medium]}"
        elif ((percentage <= display_threshold)); then
            echo -n "${PS3000_THEME[battery]}"
        fi

        if ((percentage <= display_threshold)); then
            echo "$percentage%"
        fi
    fi
}

function ps3000_nix_shell() {
    if [[ -n "$IN_NIX_SHELL" ]]; then
        echo "${PS3000_THEME[nix_shell]}$IN_NIX_SHELL"
    else
        local packages
        packages=$(echo "$PATH" | tr ':' '\n' | grep '/nix/store' | sed -nE 's=^[^-]*-(.*)/.*=\1=p' | tr '\n' ' ')
        if [[ -n "$packages" ]]; then
            echo "${PS3000_THEME[nix_shell]}$packages"
        fi
    fi
}

function ps3000_ps1() {
    ps3000__return_code=$?
    ps3000__end_time=$(date +%s)
    unset ps3000__start_time
    local last_history_with_time
    last_history_with_time="$(
        HISTTIMEFORMAT='%s '
        history 1
    )"
    if [[ -n $ps3000__not_first_prompt ]]; then
        # If the two last line of history are the same, this means that the
        # last command was not a command, but was empty
        if [ "$last_history_with_time" != "$ps3000__last_history_with_time" ]; then
            ps3000__last_history_with_time="$last_history_with_time"
            local last_history_with_time_array
            read -ra last_history_with_time_array <<<"$last_history_with_time"
            ps3000__start_time=${last_history_with_time_array[1]}
        fi
    else
        ps3000__last_history_with_time="$last_history_with_time"
    fi
    # We add the running and stopped jobs instead of just counting all the
    # jobs to avoid counting the finished jobs
    ps3000__nb_jobs=$(($(jobs -pr | wc -l) + $(jobs -ps | wc -l)))
    local full_separator="${PS3000_THEME[normal]}${PS3000_THEME[separator]}${PS3000_THEME[normal]}"
    PS1=""

    unset ps3000__not_first
    for command in "${!PS3000_COMMANDS[@]}"; do
        local content
        local return_code
        content=$(eval "${PS3000_COMMANDS[$command]}")
        return_code=$?
        if [[ "$return_code" -ne 0 ]]; then
            content="${PS3000_THEME[fail]}$return_code"
        fi
        if [[ -n "$content" ]]; then
            # if this is the first command or if the command start with a
            # new line, don't show the separator but still make sure the color are normal
            # and set ps3000__not_first so that the separator is printed for the next command
            if [[ -z $ps3000__not_first ]] || [[ "$content" == '\n'* ]]; then
                ps3000__not_first=1
                PS1+="${PS3000_THEME[normal]}"
            else
                PS1+="$full_separator"
            fi

            # if the content end with a newline, act like the next command
            # is the first and thus do not add an extraneous separator
            if [[ "$content" == *'\n' ]]; then
                unset ps3000__not_first
                PS1+="${PS3000_THEME[normal]}"
            fi

            PS1+="$content"
        fi
    done

    PS1+="${PS3000_THEME[reset]}"
    PS1+=" "
    # We expand everything we want when we generate the PS1 string, if we don't disable promptvars we get issue we escaping dollar sign and risk command injection from git branch names
    shopt -u promptvars

    ps3000__not_first_prompt=1

    # set $? back to its previous value, so that other command in $PROMPT_COMMAND can use it
    return $ps3000__return_code
}

# Only add to PROMPT_COMMAND if not already in it
if [[ ! ${PROMPT_COMMAND} =~ 'ps3000_ps1;' ]]; then
    PROMPT_COMMAND="ps3000_ps1; $PROMPT_COMMAND"
fi
# make PROMPT_DIRTRIM 3 by default
PROMPT_DIRTRIM=${PROMPT_DIRTRIM:-3}
PS3000_GIT_TIMEOUT=${PS3000_GIT_TIMEOUT:-1s}

if [[ -z $PS3000_THEME ]]; then
    declare -A PS3000_THEME=(
        [reset]="\[$(tput sgr0)\]"
        [normal]="\[$(tput sgr0)\]"
        [info]="\[$(tput setaf 6)\]"
        [warning]="\[$(tput setaf 3)\]"
        [error]="\[$(tput setaf 1)$(tput smso)\]"
        [separator]="\[$(tput bold)\] - "
        [git_ahead]="A"
        [git_behind]="B"
        [git_conflict]="C"
        [git_modified]="M"
        [git_deleted]="D"
        [git_staged]="S"
        [git_stash]="$"
        [git_untracked]="U"
    )
    PS3000_THEME[battery]="B "
    PS3000_THEME[battery_medium]="${PS3000_THEME[warning]}${PS3000_THEME[battery]}"
    PS3000_THEME[battery_low]="${PS3000_THEME[error]}${PS3000_THEME[battery]}"
    PS3000_THEME[execution_time]="${PS3000_THEME[warning]}"
    PS3000_THEME[fail]="${PS3000_THEME[error]}FAIL!!!: "
    PS3000_THEME[jobs]="${PS3000_THEME[warning]}J "
    PS3000_THEME[nix_shell]="${PS3000_THEME[info]}N "
    PS3000_THEME[read_only]="${PS3000_THEME[error]}R"
    PS3000_THEME[return_code]="${PS3000_THEME[error]}? "
    PS3000_THEME[shell_level]="${PS3000_THEME[warning]}LVL "
    PS3000_THEME[sudo_cached]="${PS3000_THEME[warning]}S"
    PS3000_THEME[venv]="${PS3000_THEME[info]}P "
fi

if [[ -z $PS3000_COMMANDS ]]; then
    declare -a PS3000_COMMANDS=(
        'echo "\n"'
        'echo "\t \u@\H"'
        ps3000_git
        ps3000_venv
        ps3000_nix_shell
        'echo "\n"'
        ps3000_return_code
        'ps3000_battery 80 30 5'
        ps3000_shell_level
        ps3000_execution_time
        ps3000_jobs
        ps3000_read_only
        ps3000_git_index_locked
        ps3000_sudo_cached
        'echo "\w \\$"'
    )
fi
