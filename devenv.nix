{ pkgs, ... }:

{
  packages = [ pkgs.shellcheck pkgs.shfmt ];

  pre-commit.hooks = {
    lint.enable = true;
    lint.entry = "${pkgs.callPackage ./lint.nix {}}/bin/lint.sh";
    lint.pass_filenames = false;
  };
}
